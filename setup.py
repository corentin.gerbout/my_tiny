from setuptools import setup, find_packages
import  pathlib

here = pathlib.Path(__file__).parent.resolve()

setup(
    name="tiny_prog",
    version="0.0.1",
    description="a tiny package for testing",
    author="Youssef El Behi",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.6, <4',
    package_data={'tiny_prog':['data.dat'],}

)